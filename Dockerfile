FROM node
WORKDIR /app
COPY package-lock.json .
RUN npm install express
COPY app.js .
EXPOSE 3000
CMD ["node","app.js"]